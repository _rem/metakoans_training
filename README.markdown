# Training on Ruby Meta-programming with MetaKoans

I've been asked a few times by clients to give Ruby meta-programming
training to other developers. The first time that this happened, I was
lucky to stumble upon [RubyQuiz #67 - metakoans.rb](http://www.rubyquiz.com/quiz67.html).

After using MetaKoans as a training tool for the first time, I realized
that, while it was great for helping people to learn Ruby
meta-programming, it didn't lend itself as easily as it could to being
solved in an incremental fashion. For details on this, see my
[blog article](http://kinderman.net/articles/2007/09/22/learning-ruby-meta-programming-with-metakoans)
on the topic.

I've rewritten the koans so that the solution can be developed more
incrementally. To train from the beginning simply:

```terminal
$ ruby meta_koans.rb
```

I've also saved the knowledge permutations that I made for each koan, to
prove that the problem can be solved in a stepwise fashion included under
the `knowledge` directory.

To replay a specific permutation:

```terminal
$ ruby meta_koans.rb [koan_num] [revision]
```

that is

```terminal
$ ruby meta_koans.rb 1 4
```

If you're looking to learn Ruby meta-programming, don't look at my
knowledge files!
