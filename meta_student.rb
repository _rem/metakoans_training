class MetaStudent
  def initialize knowledge
    require_relative knowledge
  end

  def ponder koan
    send koan
    true
  rescue => e
    STDERR.puts %Q[#{ e.message } (#{ e.class })\n#{ e.backtrace.join 10.chr }]
    false
  end
end
